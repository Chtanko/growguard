package com.growguard.customer;

public record CustomerRegistrationRequest(
        String name,
        String email,
        Integer age
){

}
