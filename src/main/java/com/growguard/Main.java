package com.growguard;

import com.growguard.customer.Customer;
import com.growguard.customer.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.List;


@SpringBootApplication
public class Main {

    public static void main(String[] args) {
                SpringApplication.run(Main.class, args);
    }

    @Bean
    CommandLineRunner runner(CustomerRepository customerRepository) {

        return args -> {

            Customer tanx  = new Customer (

                    "Tanx",
                    "boueng@gmail.com",
                    32
            );

            Customer ascent  = new Customer (

                    "Ascent",
                    "boueng@gmail.com",
                    32
            );

            List<Customer> customers = List.of(tanx, ascent);
            customerRepository.saveAll(customers);

        };
    }


}
